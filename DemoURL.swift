//
//  DemoURL.swift
//  Cassini
//
//  Created by Yijia Huang on 9/7/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import Foundation

struct DemoURL {
    static let isu = URL(string: "https://photos.smugmug.com/Other/ISUphotoservices/Front-Images/i-7jLvGVQ/0/9d87ab94/X2/546537565_dsc_0317campus-X2.jpg")
    
    static var NASA: Dictionary<String, URL> = {
        let NASAURLStrings = [
            "Cassini" : "http://www.jpl.nasa.gov/images/cassini/20090202/pia03883-full.jpg",
            "Earth" : "http://www.nasa.gov/sites/default/files/wave_earth_mosaic_3.jpg",
            "Saturn" : "http://www.nasa.gov/sites/default/files/saturn_collage.jpg"
        ]
        var urls = Dictionary<String, URL>()
        for (key, value) in NASAURLStrings {
            urls[key] = URL(string: value)
        }
        return urls
    }()
}
